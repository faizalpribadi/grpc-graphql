package main

import (
	"github.com/faizalpribadi/webapi/services/pkg/cmd"
)

func main() {
	cmd.Start()
}

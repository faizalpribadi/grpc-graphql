package title

import (
	"context"
	"net"

	"github.com/jmoiron/sqlx"

	v1 "github.com/faizalpribadi/webapi/services/pkg/api/v1/title"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type titleServiceServer struct {
	db *sqlx.DB
}

func NewTitleServiceServer(db *sqlx.DB) v1.TitleServiceServer {
	return &titleServiceServer{db: db}
}

func (s *titleServiceServer) connect(ctx context.Context) (*sqlx.DB, error) {
	_, err := s.db.Conn(ctx)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to connect to database-> "+err.Error())
	}
	return s.db, nil
}

func (s *titleServiceServer) GetTitles(ctx context.Context, req *v1.TitleRequest) (*v1.TitleResponse, error) {
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	// defer c.Close()

	rows, err := c.QueryContext(ctx, "SELECT title_id,title_name FROM titles")
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to select from titles-> "+err.Error())
	}
	// defer rows.Close()

	list := []*v1.Title{}
	for rows.Next() {
		title := new(v1.Title)
		if err := rows.Scan(&title.TitleId, &title.TitleName); err != nil {
			return nil, status.Error(codes.Unknown, "failed to retrieve field values from titles row-> "+err.Error())
		}

		list = append(list, title)
	}

	if err := rows.Err(); err != nil {
		return nil, status.Error(codes.Unknown, "failed to retrieve data from titles-> "+err.Error())
	}

	return &v1.TitleResponse{
		Titles: list,
	}, nil
}

func (s *titleServiceServer) GetTitle(ctx context.Context, req *v1.TitleRequest) (*v1.Title, error) {
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	title := &v1.Title{}
	query := `
		SELECT 
			title_id,
			title_name
		FROM
			titles
		WHRER
			title_id = ?
	`
	err = c.Select(&title, query, req.TitleId)
	if err != nil {
		return nil, err
	}

	return &v1.Title{
		TitleId:   title.TitleId,
		TitleName: title.TitleName,
	}, nil
}

func GrpcTitleServe(l net.Listener, db *sqlx.DB) error {
	s := grpc.NewServer()
	v1.RegisterTitleServiceServer(s, &titleServiceServer{db})

	return s.Serve(l)
}

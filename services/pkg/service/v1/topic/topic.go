package v1

import (
	"context"
	"net"

	v1 "github.com/faizalpribadi/webapi/services/pkg/api/v1/topic"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type topicServiceServer struct {
	db *sqlx.DB
}

func NewTopicServiceServer(db *sqlx.DB) v1.TopicServiceServer {
	return &topicServiceServer{db: db}
}

func (s *topicServiceServer) connect(ctx context.Context) (*sqlx.DB, error) {
	_, err := s.db.Conn(ctx)
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to connect to database-> "+err.Error())
	}
	return s.db, nil
}

func (s *topicServiceServer) GetTopics(ctx context.Context, req *v1.TopicRequest) (*v1.TopicResponse, error) {
	c, err := s.connect(ctx)
	if err != nil {
		return nil, err
	}
	// defer c.Close()

	rows, err := c.QueryContext(ctx, "SELECT topic_id,topic_name FROM topics")
	if err != nil {
		return nil, status.Error(codes.Unknown, "failed to select from ToDo-> "+err.Error())
	}
	// defer rows.Close()

	list := []*v1.Topic{}
	for rows.Next() {
		topic := new(v1.Topic)
		if err := rows.Scan(&topic.TopicId, &topic.TopicName); err != nil {
			return nil, status.Error(codes.Unknown, "failed to retrieve field values from ToDo row-> "+err.Error())
		}

		list = append(list, topic)
	}

	if err := rows.Err(); err != nil {
		return nil, status.Error(codes.Unknown, "failed to retrieve data from ToDo-> "+err.Error())
	}

	return &v1.TopicResponse{
		Topics: list,
	}, nil
}

func GrpcTopicServe(l net.Listener, db *sqlx.DB) error {
	s := grpc.NewServer()
	v1.RegisterTopicServiceServer(s, &topicServiceServer{db})

	return s.Serve(l)
}

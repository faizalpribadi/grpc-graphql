package grpc

import (
	"log"
	"net"

	"github.com/jmoiron/sqlx"
	"github.com/soheilhy/cmux"

	title "github.com/faizalpribadi/webapi/services/pkg/service/v1/title"
	topic "github.com/faizalpribadi/webapi/services/pkg/service/v1/topic"
)

func RunGrpcServer(db *sqlx.DB) error {

	listen, err := net.Listen("tcp", ":50080")
	if err != nil {
		log.Fatal(err)
	}

	m := cmux.New(listen)
	grpcListener := m.Match(cmux.HTTP2HeaderField("content-type", "application/grpc"))

	go title.GrpcTitleServe(grpcListener, db)
	go topic.GrpcTopicServe(grpcListener, db)

	log.Println("starting gRPC server...")

	return m.Serve()

	// server := grpc.NewServer()
	// v1.RegisterTopicServiceServer(server, topic)

	// // graceful shutdown
	// c := make(chan os.Signal, 1)
	// signal.Notify(c, os.Interrupt)
	// go func() {
	// 	for range c {
	// 		// sig is a ^C, handle it
	// 		log.Println("shutting down gRPC server...")

	// 		server.GracefulStop()

	// 		<-ctx.Done()
	// 	}
	// }()

	// // start gRPC server
	// log.Println("starting gRPC server...")
	// return server.Serve(listen)
}

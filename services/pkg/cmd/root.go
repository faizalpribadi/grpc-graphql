package cmd

import (
	"log"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{}

func Start() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

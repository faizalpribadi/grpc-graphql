package cmd

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/faizalpribadi/webapi/services/pkg/protocol/grpc"
	"github.com/jmoiron/sqlx"

	"github.com/spf13/cobra"

	_ "github.com/go-sql-driver/mysql"
)

const (
	port = "50080"
)

var serverCmd = &cobra.Command{
	Use: "server",
	Run: run,
}

func init() {
	rootCmd.AddCommand(serverCmd)
}

func run(cmd *cobra.Command, args []string) {
	// ctx := context.Background()

	db, err := sqlx.Open("mysql", os.Getenv("DB"))
	if err != nil {
		fmt.Errorf("failed to open database: %v", err)
	}

	// defer db.Close()

	db.SetMaxIdleConns(5)
	db.SetConnMaxLifetime(2 * time.Minute)
	db.SetMaxOpenConns(95)
	if err != nil {
		log.Println("m=GetPool,msg=connection has failed", err)
	}

	if err := grpc.RunGrpcServer(db); err != nil {
		log.Panic(err)
	}
}

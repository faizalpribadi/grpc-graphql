let grpc = require("grpc");
var protoLoader = require("@grpc/proto-loader");
const path = require('path');

const protoPath = path.join(__dirname, '/../api/proto/v1/')
//Load the protobuf
var proto = grpc.loadPackageDefinition(
  protoLoader.loadSync(protoPath + "topic.proto", {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
  })
);

const REMOTE_SERVER = "0.0.0.0:50080";

//Create gRPC client
let topicClient = new proto.topic.TopicService(
  REMOTE_SERVER,
  grpc.credentials.createInsecure()
);

module.exports = topicClient;
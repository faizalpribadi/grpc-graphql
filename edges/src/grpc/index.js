const topic = require('./topic');
const title = require('./title');

module.exports = {
    topic,
    title,
}
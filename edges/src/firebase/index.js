const firebase = require("firebase-admin");

const config = {
  apiKey: "AIzaSyCoS72Vscm_IeDweaNb5z5bJXZLVWSznU4",
  authDomain: "infonesia-81671.firebaseapp.com",
  databaseURL: "https://infonesia-81671.firebaseio.com",
  projectId: "infonesia-81671",
  storageBucket: "infonesia-81671.appspot.com",
  messagingSenderId: "844995515314"
};

module.exports = firebase.initializeApp(config);

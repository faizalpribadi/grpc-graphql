const { gql } = require('apollo-server-express');

const typeDefs = gql`
    type Topic {
        topic_id: String!
        topic_name: String!
    }

    type Title {
        title_id: String!
        title_name: String!
    }

    type Query {
        topics: [Topic!]
        titles: [Title!]
    }
`

module.exports = typeDefs
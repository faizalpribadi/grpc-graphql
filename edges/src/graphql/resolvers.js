const { AuthenticationError } = require("apollo-server-express");
const { topic, title } = require("../grpc");

const getTopics = () => {
  return new Promise((resolve, reject) => {
    topic.GetTopics({}, (error, response) => {
      if (error) {
        return reject(error);
      }

      return resolve(response);
    });
  });
};

const getTitles = () => {
  return new Promise((resolve, reject) => {
    title.GetTitles({}, (error, response) => {
      if (error) {
        return reject(error);
      }

      return resolve(response);
    });
  });
};

const resolvers = {
  Query: {
    topics: (root, args, context) => {
      const { error } = context;
      if (error) {
        throw new AuthenticationError(error);
      }

      return getTopics()
        .then(data => {
          return data.topics;
        })
        .catch(error => {
          console.log(error)
          return error;
        });
    },
    titles: (root, args, context) => {
      const { error } = context;
      if (error) {
        throw new AuthenticationError(error);
      }

      return getTitles()
        .then(data => {
          return data.titles;
        })
        .catch(error => {
          console.log(error)
          return error;
        });
    }
  }
};

module.exports = resolvers;

const { ApolloServer } = require("apollo-server-express");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const compression = require("compression");
const helmet = require("helmet");

const graphql = require("./graphql");
// const firebase = require("./firebase");


const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(compression());
app.use(helmet());

const server = new ApolloServer({
  typeDefs: graphql.typeDefs,
  resolvers: graphql.resolvers,
  context: ({ req }) => {
    // if (!req.get("x-web-app")) {
    //   return {
    //     error: "invalid header authentication"
    //   };
    // }
  }
});

server.applyMiddleware({ app });

module.exports = {
  start: () => {
    app.listen(4000);
  }
};
